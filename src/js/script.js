$(document).ready(function () {

  /**
   * Show and close mobile menu
   */
  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
    $('body').removeClass('s-prevent-scroll');
  });



  /**
   * Show and close search block
   */
  $('.s-page-header__search-btn').on('click', function () {
    $('.s-search').addClass('s-search--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-search__close').on('click', function () {
    $('.s-search').removeClass('s-search--is-active');
    $('body').removeClass('s-prevent-scroll');
  });



  window.addEventListener('resize', () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });


  var popularArticlesSlider = new Swiper('.s-popular-articles__slider .swiper-container', {
    navigation: {
      nextEl: '.s-popular-articles__slider-nav-btn--next',
      prevEl: '.s-popular-articles__slider-nav-btn--prev',
    },
    spaceBetween: 20,
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 4
      }
    }
  });



  var promoSwiperThumbs = new Swiper('.s-promo__slider-thumbs-inner .swiper-container', {
    spaceBetween: 20,
    slidesPerView: 4,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    speed: 750,
    loop: true,
  });

  var promoSwiperMain = new Swiper('.s-promo__slider-main.swiper-container', {
    speed: 750,
    loop: true,
    parallax: true,
    centeredSlides: true,
    autoplay: true,
    thumbs: {
      swiper: promoSwiperThumbs
    },
    navigation: {
      nextEl: '.s-promo__slider-nav-btn--next',
      prevEl: '.s-promo__slider-nav-btn--prev',
    },
    on: {
      init: function () {
        let swiper = this;
        for (let i = 0; i < swiper.slides.length; i++) {
          $(swiper.slides[i])
            .find('.s-promo__slide-cover')
            .attr({
              'data-swiper-parallax': 0.875 * swiper.width,
              'data-swiper-paralalx-opacity': 0.25
            });
          $(swiper.slides[i])
            .find('.s-promo__slide-label')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
          $(swiper.slides[i])
            .find('.s-promo__slide-title')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
          $(swiper.slides[i])
            .find('.s-promo__slide-desc')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
          $(swiper.slides[i])
            .find('.s-promo__slide-btn')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
        }
      },
      resize: function () {
        this.update();
      }
    }
  });



  $(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
      $('.s-page-header').addClass('s-page-header--compact');
    } else {
      $('.s-page-header').removeClass('s-page-header--compact');
    }
  });



  $(window).on('scroll', function () {
    scrollHeaderIndicator()
    scrollPercentageIndicator()
  });

  function scrollHeaderIndicator () {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.querySelector('.s-page-header__scroll-indicator-active').style.width = scrolled + "%";
  };

  function scrollPercentageIndicator () {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    var scrollPercentRounded = Math.round(scrolled * 1);
    $('.s-to-top__scroll-percentage').html(scrollPercentRounded + '%');
  };
});